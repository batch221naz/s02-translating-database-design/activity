-- Run mySQL and Apache in XAMPP
-- open Terminal/Command line and run the following command:
	-- "mysql -u root" for windows
	-- "mysql -u root -p" for macOS

-- commands are not case-sensitive

-- semicolon is important since it signifies the end of the command in mySQL. failure to add this would result to line break

-- List down the databases inside the DBMS
SHOW DATABASES; 

-- Create a database
CREATE DATABASE music_db;

-- Remove database
DROP DATABASE music_db;

-- Select Database
USE music_db;

-- Create Tables
-- Table columns have the following format: [column_name] [data_type] [other_options]
	-- INT - specifies that the column should accept integer data type
	-- VARCHAR(character_limit) to avoid data bloating, commonly used for strings
	-- DATE refers to YYYY-MM-DD
	-- TIME refers to HH:MM:SS
	-- DATETIME refers to  YYYY-MM-DD HH:MM:SS
	-- NOT NULL means it is required
	-- AUTO_INCREMENT setting the id to have different value (1 -> 2 -> 3 -> ...)
	-- PRIMARY KEY (column_name) - unique identifier for each row in the table

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);
/*
	Miniactivity 7 mins
		1. create a table for artists
		2. artists should have an id
		3. artists is required to have a name with 50 characters limit
		4. assign the primary key to its id
		5. send the screenshot of the phpmyadmin in Hangouts/Google Chat
	7:45 pm - solution discussion
*/
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- CONSTRAINT is a command rule, this is optional, in this context we used it to identify the FOREIGN KEY
-- FOREIGN KEY is the connection between tables
-- REFERENCES refers to where the FK gets its value
-- ON UPDATE CASCADE, every time the parent table updates, it should also affect the related table
-- ON DELETE RESTRICT, the tables cannot be immediately deleted when there are more tables related to it
CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT	NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

/*
	Miniactivity 10 minutes: 8:40 pm
		1. create a table for songs
		2. put a required auto increment id
		3. declare a song name with 50 char limit, this should be required
		4. declare a length with the data type time and this should be required
		5. declare a genre with 50 char limit, this should be required
		6. declare an integer as album id that should be required
		7. create a primary key referring to the id of the songs
		8. create a foreign key and name it fk_songs_album_id
			8.1. this should be referred to the album id
			8.2. it should have cascaded update and restricted delete
		9. run create table songs
		10. send the output in the Hangouts/Google Chat
*/

CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	date_time_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);